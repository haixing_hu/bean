/**
 * Copyright (c) 2014, Haixing Hu
 *
 * All rights reserved.
 */
package org.bitbucket.haixing_hu.bean;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.haixing_hu.lang.StringUtils;

import static org.bitbucket.haixing_hu.lang.Argument.requireNonNull;

/**
 *  The builder of the {@link PropertyDescriptorGroup} class.
 *
 * @author Haixing Hu
 */
public final class PropertyDescriptorGroupBuilder {

  private String name;
  private final List<PropertyDescriptor> descriptors;

  public PropertyDescriptorGroupBuilder() {
    name = StringUtils.EMPTY;
    descriptors = new ArrayList<>();
  }

  public PropertyDescriptorGroupBuilder setName(final String name) {
    this.name = requireNonNull("name", name);
    return this;
  }

  public PropertyDescriptorGroupBuilder add(final PropertyDescriptor descriptor) {
    descriptors.add(requireNonNull("descriptor", descriptor));
    return this;
  }

  public PropertyDescriptorGroup build() {
    return new PropertyDescriptorGroup(name,
        descriptors.toArray(new PropertyDescriptor[0]));
  }
}
