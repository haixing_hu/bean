/*
 * Copyright (C) 2014 Haixing Hu
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.bitbucket.haixing_hu.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.bitbucket.haixing_hu.lang.Equality;
import org.bitbucket.haixing_hu.lang.Hash;
import org.bitbucket.haixing_hu.lang.StringUtils;
import org.bitbucket.haixing_hu.text.tostring.ToStringBuilder;

import static org.bitbucket.haixing_hu.lang.Argument.requireNonNull;

/**
 * A {@link PropertyDescriptorGroup} represents a group of property descriptors.
 *
 * <h2>XML Serialization</h2> A {@link PropertyDescriptorGroup} instance can be
 * serialized to and from XML. The XML representation of a
 * {@link PropertyDescriptor} has the following shape:
 * <pre>
 * <code>
 * &lt;property-group name='group1'&gt;
 *   &lt;property name='prop1' type='string' kind='simple'/&gt;
 *   &lt;property name='prop2' type='int' kind='indexed'/&gt;
 *   &lt;property name='prop3' type='my-bean' kind='mapped'/&gt;
 * &lt;/property-group&gt;
 * </code>
 * </pre>
 *
 * @author Haixing Hu
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "group")
public final class PropertyDescriptorGroup {

  @XmlAttribute(name="name", required = true)
  private final String name;

  @XmlElement(name = "property")
  private final PropertyDescriptor[] descriptors;

  /**
   * Default constructor used by the JAXB.
   */
  protected PropertyDescriptorGroup() {
    name = StringUtils.EMPTY;
    descriptors = new PropertyDescriptor[0];
  }

  /**
   * Constructs a {@link PropertyDescriptorGroup}.
   *
   * @param name
   *          the name of the group, which cannot be {@code null}. The names
   *          of a group must satisfy the same requirement as the names of
   *          a property.
   * @param descriptors
   *          the array of property descriptors in the group, which cannot be
   *          {@code null}.
   * @see PropertyDescriptor#isValidName(String)
   */
  public PropertyDescriptorGroup(final String name,
      final PropertyDescriptor[] descriptors) {
    this.name = requireNonNull("name", name);
    if (! PropertyDescriptor.isValidName(name)) {
      throw new IllegalArgumentException("Invalid group name: " + name);
    }
    this.descriptors = requireNonNull("descriptors", descriptors);
  }

  /**
   * Gets the name of this group.
   *
   * @return the name of this group, which will never be {@code null}.
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the list of property descriptors in this group.
   *
   * @return the array of property descriptors in this group, which will never be
   *         {@code null}.
   */
  public PropertyDescriptor[] getDescriptors() {
    return descriptors;
  }

  @Override
  public int hashCode() {
    final int multiplier = 3;
    int code = 17;
    code = Hash.combine(code, multiplier, name);
    code = Hash.combine(code, multiplier, descriptors);
    return code;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    final PropertyDescriptorGroup rhs = (PropertyDescriptorGroup) obj;
    return Equality.equals(name, rhs.name)
        && Equality.equals(descriptors, rhs.descriptors);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
              .append("name", name)
              .append("descriptors", descriptors)
              .toString();
  }

}
