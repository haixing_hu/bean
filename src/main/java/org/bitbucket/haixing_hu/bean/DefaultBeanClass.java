/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.bean;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.bitbucket.haixing_hu.lang.Equality;
import org.bitbucket.haixing_hu.lang.Hash;
import org.bitbucket.haixing_hu.lang.StringUtils;
import org.bitbucket.haixing_hu.reflect.ReflectionException;
import org.bitbucket.haixing_hu.text.tostring.ToStringBuilder;

import static org.bitbucket.haixing_hu.lang.Argument.requireNonNull;

/**
 * A default implementation of {@link BeanClass} interface.
 * <p>
 * <h2>XML Serialization</h2>
 * An example XML representation of a {@link DefaultBeanClass} is as follows:
 * <pre><code>
 * &lt;bean-class&gt;
 *   &lt;name&gt;bean2&lt;/name&gt;
 *   &lt;bean&gt;default-bean&lt;/bean&gt;
 *   &lt;properties&gt;
 *     &lt;property name='prop1' type='string' kind='simple'/&gt;
 *     &lt;property name='prop2' type='int' kind='indexed'/&gt;
 *     &lt;property name='prop3' type='my-bean' kind='mapped'/&gt;
 *   &lt;/properties&gt;
 * &lt;/bean-class&gt;
 * </code></pre>
 *
 * @author Haixing Hu
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "bean-class")
public class DefaultBeanClass implements BeanClass {

  /**
   * The name of this bean class.
   */
  @XmlElement(name = "name", required = true)
  protected final String name;

  /**
   * The {@link Bean} implementation class used for creating new instances.
   */
  @XmlJavaTypeAdapter(TypeAliasXmlAdapter.class)
  @XmlElement(name = "bean", required = false)
  protected Class<? extends Bean> beanType = null;

  /**
   * The property descriptors of the beans created by this bean class.
   */
  @XmlElementWrapper(name = "properties", required = true)
  @XmlElement(name = "property")
  protected PropertyDescriptor[] descriptors = {};

  /**
   * A map from the property name to its descriptor.
   */
  protected transient Map<String, PropertyDescriptor> descriptorMap = null;

  /**
   * The constructor of the {@code beanType} used for creating new instances.
   */
  protected transient Constructor<? extends Bean> constructor = null;

  /**
   * A default constructor used by the JAXB.
   */
  protected DefaultBeanClass() {
    name = StringUtils.EMPTY;
    beanType = null;
  }

  /**
   * Constructs a {@link DefaultBeanClass}.
   * <p>
   * The constructor will use {@link BasicBean.class} to create new bean
   * instances.
   *
   * @param name
   *          the name of the new bean class.
   * @param descriptors
   *          the property descriptors for the properties of the beans created
   *          by the new bean class.
   */
  public DefaultBeanClass(final String name,
      final PropertyDescriptor[] descriptors) {
    this(name, descriptors, null);
  }

  /**
   * Constructs a {@link DefaultBeanClass}.
   *
   * @param name
   *          the name of the new bean class.
   * @param descriptors
   *          the property descriptors for the properties of the beans created
   *          by the new bean class.
   * @param beanType
   *          {@link Bean} implementation class used for creating new bean
   *          instances. If this argument is {@code null}, the constructor will
   *          use {@link BasicBean.class} to create new bean instances.
   */
  public DefaultBeanClass(final String name,
      final PropertyDescriptor[] descriptors,
      @Nullable final Class<? extends Bean> beanType) {
    this.name = requireNonNull("name", name);
    this.descriptors = requireNonNull("descriptors", descriptors);
    if ((beanType != null) && beanType.isInterface()) {
      throw new IllegalArgumentException("Class " + beanType.getName()
          + " is an interface, not a class");
    }
    this.beanType = beanType;
    updateDescriptors();
    updateBeanType();
  }

  /**
   * Updates the property descriptors of the beans created by this bean class.
   * <p>
   * This function must be called after changing the property descriptors.
   */
  protected void updateDescriptors() {
    if (descriptorMap == null) {
      descriptorMap = new HashMap<>();
    } else {
      descriptorMap.clear();
    }
    for (final PropertyDescriptor descriptor : descriptors) {
      descriptorMap.put(descriptor.getName(), descriptor);
    }
  }

  /**
   * Updates the type of the beans created by this bean class.
   * <p>
   * This function must be called after changing the bean type.
   */
  protected void updateBeanType() {
    // Identify the Constructor we will use in newInstance()
    Class<? extends Bean> type = this.beanType;
    if (type == null) {
      type = DefaultBean.class;
    }
    final Class<?>[] signature = new Class<?>[]{ this.getClass() };
    try {
      constructor = type.getConstructor(signature);
    } catch (final NoSuchMethodException e1) {
      try {
        signature[0] = BeanClass.class;
        constructor = type.getConstructor(signature);
      } catch (final NoSuchMethodException e2) {
        throw new IllegalArgumentException("Class " + this.beanType.getName()
            + " does not have an appropriate constructor");
      }
    }
  }

  @Override
  public final String getName() {
    return name;
  }

  @Override
  public Class<? extends Bean> getBeanType() {
    return (beanType != null ? beanType : DefaultBean.class);
  }

  @Override
  public final PropertyDescriptor[] getPropertyDescriptors() {
    return descriptors;
  }

  @Override
  public final boolean hasProperty(final String name) {
    if (descriptorMap == null) {
      updateDescriptors();
    }
    return descriptorMap.containsKey(name);
  }

  @Override
  public final PropertyDescriptor getPropertyDescriptor(final String name) {
    if (descriptorMap == null) {
      updateDescriptors();
    }
    return descriptorMap.get(name);
  }

  @Override
  public Bean newInstance() {
    if (constructor == null) {
      updateBeanType();
    }
    try {
      return constructor.newInstance(this);
    } catch (final Exception e) {
      throw new ReflectionException(e);
    }
  }

  @Override
  public int hashCode() {
    final int multiplier = 13;
    int code = 7;
    code = Hash.combine(code, multiplier, name);
    code = Hash.combine(code, multiplier, beanType);
    code = Hash.combine(code, multiplier, descriptors);
    return code;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    final DefaultBeanClass rhs = (DefaultBeanClass) obj;
    return Equality.equals(name, rhs.name)
        && Equality.equals(beanType, rhs.beanType)
        && Equality.equals(descriptors, rhs.descriptors);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
              .append("name", name)
              .append("beanType", beanType)
              .append("descriptors", descriptors)
              .toString();
  }

}
